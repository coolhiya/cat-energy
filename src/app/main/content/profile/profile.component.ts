import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../auth/auth.service';

@Component({
    selector: 'ari-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

    constructor(
        private readonly _route: Router,
        private readonly _authService: AuthService
    ) { }

    ngOnInit() {
    }

    onLoggedOut() {
        this._authService.logout();
        this._route.navigate(['/']);
    }

}
