import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';

import { ProductsComponent } from './products.component';
import { ProductListComponent } from './product-list';

const productsRoutes: Routes = [
    {
        path: '', component: ProductsComponent, children: [
            { path: '', component: ProductListComponent },
            { path: ':type', component: ProductListComponent }
        ]
    }
]

@NgModule({
    imports: [
        RouterModule.forChild(productsRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class ProductsRoutingModule {

}