import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FilterModule } from 'src/app/core/pipes/filter';

import { ProductsComponent } from './products.component';
import { ProductsRoutingModule } from './products-routing.module';
import { RouterModule } from '@angular/router';
import { ProductListComponent } from './product-list';


@NgModule({
    declarations: [
        ProductsComponent,
        ProductListComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        ProductsRoutingModule,
        FilterModule
    ]
})
export class ProductsModule { }
