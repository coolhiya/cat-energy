import { Component, OnInit } from '@angular/core';
import { BaseProduct } from '../base-product.model';
import { ProductsService } from '../products.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'ari-product-list',
    templateUrl: './product-list.component.html',
    styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
    baseProducts: BaseProduct[];
    filter: string;

    constructor(
        private readonly _productsService: ProductsService,
        private _route: ActivatedRoute,
        private _router: Router
    ) { }

    ngOnInit() {
        this.baseProducts = this._productsService.getBaseProducts();
        this.filter = this._route.snapshot.params.type;
    }

    onShownAllProducts() {
        this._router.navigate(['/products']);
    }

    filterByType(product: BaseProduct, filter: string) {
        return product.type === filter;
    }
}
