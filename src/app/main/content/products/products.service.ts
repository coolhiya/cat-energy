import { BaseProduct } from './base-product.model';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ProductsService {
    private _baseProducts: BaseProduct[];

    constructor() {
        this._baseProducts = [
            new BaseProduct(1, 'Cat energy pro', 'pro', 500, 'Курица', 700),
            new BaseProduct(2, 'Cat energy pro', 'pro', 1000, 'Курица', 1000),
            new BaseProduct(3, 'Cat energy pro', 'pro', 500, 'Рыба', 700),
            new BaseProduct(4, 'Cat energy pro', 'pro', 1000, 'Рыба', 1000),
            new BaseProduct(5, 'Cat energy slim', 'slim', 500, 'Гречка', 400),
            new BaseProduct(6, 'Cat energy slim', 'slim', 1000, 'Рис', 700),
        ]
    }

    getBaseProducts() {
        return this._baseProducts.slice();
    }
}