export class BaseProduct {
    constructor(
        public id: number,
        public name: string,
        public mass: number,
        public price: number
    ) { }
}