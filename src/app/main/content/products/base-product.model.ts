export class BaseProduct {
    constructor(
        public id: number,
        public name: string,
        public type: string,
        public mass: number,
        public taste: string,
        public price: number,
        public imgPath?: string
    ) { }
}