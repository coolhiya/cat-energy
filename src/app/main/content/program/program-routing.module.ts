import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';

import { ProgramComponent } from './program.component';

const programRoutes: Routes = [
    { path: '', component: ProgramComponent }
]

@NgModule({
    imports: [
        RouterModule.forChild(programRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class ProgramRoutingModule {

} 