import {
    Component,
    OnInit,
    ViewEncapsulation,
    HostBinding,
    Input
} from '@angular/core';
import { Feature } from '../../feature';

@Component({
    selector: 'ari-feature',
    templateUrl: './feature.component.html',
    styleUrls: ['./feature.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class FeatureComponent implements OnInit {

    @Input() feature: Feature;

    @HostBinding('class.feature') get cssFeature() {
        return true;
    }

    constructor() { }

    ngOnInit() {
    }

}
