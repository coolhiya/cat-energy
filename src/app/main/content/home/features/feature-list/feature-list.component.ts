import {
    Component,
    OnInit,
    ViewEncapsulation,
    HostBinding
} from '@angular/core';
import { Feature } from '../feature';

@Component({
    selector: 'ari-feature-list',
    templateUrl: './feature-list.component.html',
    styleUrls: ['./feature-list.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class FeatureListComponent implements OnInit {
    features: Feature[];

    @HostBinding('class.feature-list') get cssFeatureList() {
        return true;
    }

    constructor() {
        this.features = [
            { icon: '', description: 'Функциональное питание содержит только полезные питательные вещества.' },
            { icon: '', description: 'Выпускается в виде порошка, который нужно лишь залить кипятком и готово.' },
            { icon: '', description: 'Замените один-два приема обычной еды на наше фунцкциональное питание.' },
            { icon: '', description: 'Уже через месяц наслаждайтесь изменениями к лучшему вашего питомца!' }
        ]
    }

    ngOnInit() {
    }

}
