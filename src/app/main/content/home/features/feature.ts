export interface Feature {
    icon: string,
    description: string;
}