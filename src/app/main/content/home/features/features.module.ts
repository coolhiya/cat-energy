import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FeatureListComponent, FeatureComponent } from './feature-list/index';
import { FeaturesComponent } from './features.component';


@NgModule({
    declarations: [
        FeatureListComponent,
        FeatureComponent,
        FeaturesComponent
    ],
    imports: [
        CommonModule
    ],
    exports: [
        FeaturesComponent
    ]
})
export class FeaturesModule { }
