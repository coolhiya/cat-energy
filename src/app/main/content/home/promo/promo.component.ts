import {
    Component,
    OnInit,
    ViewEncapsulation,
    HostBinding,
    OnDestroy
} from '@angular/core';
import { Subject } from 'rxjs/internal/Subject';
import { takeUntil } from 'rxjs/operators';

import { AuthService } from 'src/app/main/auth/auth.service';

@Component({
    selector: 'ari-promo',
    templateUrl: './promo.component.html',
    styleUrls: ['./promo.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class PromoComponent implements OnInit, OnDestroy {
    private readonly _destroy$: Subject<any>;
    isUserAuthenticated: boolean;

    @HostBinding('class.promo') get cssHome() {
        return true;
    }

    constructor(private readonly _authService: AuthService) {
        this.isUserAuthenticated = false;
        this._destroy$ = new Subject();
    }

    ngOnInit() {
        this._authService.isUserAuthenticated
            .pipe(takeUntil(this._destroy$))
            .subscribe((r) => this.isUserAuthenticated = r);
    }

    ngOnDestroy() {
        this._destroy$.next();
    }
}
