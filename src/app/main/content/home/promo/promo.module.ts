import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { PromoComponent } from './promo.component';


@NgModule({
    declarations: [
        PromoComponent
    ],
    imports: [
        CommonModule,
        RouterModule
    ],
    exports: [
        PromoComponent
    ]
})
export class PromoModule { }
