import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { FeaturesModule } from './features/index';
import { PromoModule } from './promo/index';
import { ResultModule } from './result/index';

import { HomeComponent } from './home.component';


@NgModule({
    declarations: [
        HomeComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        FeaturesModule,
        PromoModule,
        ResultModule
    ]
})
export class HomeModule { }
