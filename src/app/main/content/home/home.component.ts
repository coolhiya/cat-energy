import {
    Component,
    OnInit,
    ViewEncapsulation,
    HostBinding
} from '@angular/core';

@Component({
    selector: 'ari-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {

    @HostBinding('class.home') get cssHome() {
        return true;
    }

    constructor() {

    }

    ngOnInit() {
    }

}
