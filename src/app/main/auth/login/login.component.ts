import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../auth.service';

@Component({
    selector: 'ari-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    constructor(private readonly _authService: AuthService) { }

    ngOnInit() {
    }

    onSignUp(form: NgForm) {
        const email = form.value.email;
        const password = form.value.password;

        this._authService.signUpUser(email, password);
    }

    onSignIn(form: NgForm) {
        const email = form.value.email;
        const password = form.value.password;

        this._authService.signInUser(email, password);
    }
}
