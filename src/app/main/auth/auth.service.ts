import * as firebase from 'firebase';
import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    isUserAuthenticated: Subject<boolean>;

    constructor() {
        this.isUserAuthenticated = new Subject();
    }

    signUpUser(email: string, password: string) {
        firebase.auth().createUserWithEmailAndPassword(email, password).catch(
            error => console.error(error)
        );
    }

    signInUser(email: string, password: string) {
        firebase.auth().signInWithEmailAndPassword(email, password).then(
            response => {
                console.log(response);
                this.isUserAuthenticated.next(true);
            }
        ).catch(
            error => console.log(error)
        )
    }

    logout() {
        firebase.auth().signOut();
        this.isUserAuthenticated.next(false);
    }

}