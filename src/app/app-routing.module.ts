import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './main/content/home/home.component';

const appRoutes: Routes = [
    { path: 'products', loadChildren: './main/content/products/products.module#ProductsModule' },
    { path: 'program', loadChildren: './main/content/program/program.module#ProgramModule' },
    { path: 'profile', loadChildren: './main/content/profile/profile.module#ProfileModule' },
    { path: '', component: HomeComponent },
    { path: '**', redirectTo: '' }
]

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }