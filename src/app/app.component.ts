import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    ngOnInit() {
        firebase.initializeApp({
            apiKey: "AIzaSyCagyHERlYGH6fqjGe9Sqa8khwEtVyUG_I",
            authDomain: "cat-energy-ef7fe.firebaseapp.com",
        });
    }
}
