import {
    transition,
    style,
    trigger,
    animate
} from '@angular/animations';

export const DYNAMIC_HEIGHT = trigger('dynamicHeight', [
    transition('void => *', [
        style({ height: 0 }),
        animate(250, style({ height: '*' }))
    ]),
    transition('* => void', [
        style({ height: '*' }),
        animate(250, style({ height: 0 }))
    ])
]);