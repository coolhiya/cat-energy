import {
    Directive,
    ViewContainerRef
} from '@angular/core';

@Directive({
    selector: '[ariModalContent]'
})
export class ModalContentDirective {

    constructor(
        public viewContainerRef: ViewContainerRef
    ) { }

}
