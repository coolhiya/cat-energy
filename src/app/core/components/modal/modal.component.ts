import {
    Component,
    OnInit,
    ViewEncapsulation,
    HostBinding,
    Input,
    ViewChild,
    OnDestroy,
    ComponentFactoryResolver
} from '@angular/core';

import { ModalService } from './modal.service';
import { ModalContentDirective } from './modal-content.directive';

@Component({
    selector: 'ari-modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ModalComponent implements OnInit, OnDestroy {
    component: any;

    @ViewChild(ModalContentDirective) contentArea: ModalContentDirective;

    @HostBinding('class.modal') get cssModal() {
        return true;
    }

    constructor(
        private readonly _modal: ModalService,
        private readonly _componentFactoryResolver: ComponentFactoryResolver
    ) { }

    ngOnInit() {
        this.component = this._modal.component;
        this.onComponentChanged(this.component);

        this._modal.componentChanged
            .subscribe((component: any) => this.onComponentChanged(component))
    }

    ngOnDestroy() {

    }

    onModalClosed() {
        this._modal.close();
    }

    private onComponentChanged(component: any) {
        let componentFactory = this._componentFactoryResolver.resolveComponentFactory(component);
        let viewContainerRef = this.contentArea.viewContainerRef;
        viewContainerRef.clear();

        let componentRef = viewContainerRef.createComponent(componentFactory);
    }

}
