import {
    Injectable
} from '@angular/core';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';

import { ModalComponent } from './modal.component';

@Injectable({
    providedIn: 'root'
})
export class ModalService {
    private readonly _outlet: string;
    private _path: string;

    isOpen: boolean;
    component: any;
    componentInstance: any;
    componentChanged: Subject<any>;

    constructor(
        private readonly _router: Router
    ) {
        this.isOpen = false;
        this.componentChanged = new Subject();
        this._outlet = "modal";
    }

    init(component: any, path: string) {
        this.component = component;
        this._path = path;
        this.componentChanged.next(component);
    }

    open() {
        let route = this._router.config.filter(r => r.path === this._path && r.outlet === this._outlet)[0];
        if (!route) {
            this._router.config.unshift({ path: this._path, outlet: this._outlet, component: ModalComponent})
        }

        this._router.navigate([{ outlets: { [this._outlet]: this._path } }]);
        this.isOpen = true;
    }

    close() {
        this._router.navigate([{ outlets: { modal: null } }]);
        this.isOpen = false;
    }
}