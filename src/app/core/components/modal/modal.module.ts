import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalComponent } from './modal.component';
import { ModalContentDirective } from './modal-content.directive';


@NgModule({
    declarations: [
        ModalComponent,
        ModalContentDirective
    ],
    imports: [
        CommonModule
    ],
    exports: [
        ModalComponent
    ],
    entryComponents:[
        ModalComponent
    ]
})
export class ModalModule { }
