import {
    Component,
    OnInit,
    ViewEncapsulation,
    OnDestroy,
    HostBinding
} from '@angular/core';


@Component({
    selector: 'ari-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class HeaderComponent implements OnInit {

    @HostBinding('class.header') get cssHeader() {
        return true;
    }

    constructor() { }

    ngOnInit() {

    }
}
