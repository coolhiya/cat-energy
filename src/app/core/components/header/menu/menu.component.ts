import {
    Component,
    OnInit,
    OnDestroy,
    HostBinding
} from '@angular/core';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { AuthService, LoginComponent } from 'src/app/main/auth';
import { ModalService } from 'src/app/core/components/modal';
import { DYNAMIC_HEIGHT } from 'src/app/core/animations/index';

import { MenuLink } from './menu-link';

@Component({
    selector: 'ari-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss'],
    animations: [DYNAMIC_HEIGHT]
})
export class MenuComponent implements OnInit, OnDestroy {
    private _stop$: Subject<any>;

    isUserAuthenticated: boolean;
    menuLinks: MenuLink[];
    isMenuVisible: boolean;

    @HostBinding('class.menu') get cssMenu() {
        return true;
    }

    constructor(
        private readonly _authService: AuthService,
        private readonly _router: Router,
        private readonly _modal: ModalService
    ) {
        this.isMenuVisible = false;
        this._stop$ = new Subject();
        this.menuLinks = [
            { id: 1, name: 'Главная', path: '/' },
            { id: 2, name: 'Каталог продукции', path: '/products' },
            { id: 3, name: 'Подбор программы', path: '/program' },
            { id: 4, name: 'Профиль', path: '/profile' },
            { id: 5, name: 'Авторизация', path: '' }
        ]
    }

    ngOnInit() {
        this._modal.init(LoginComponent, 'auth');
        this._authService.isUserAuthenticated
            .pipe(takeUntil(this._stop$))
            .subscribe((isUserAuthenticated: boolean) =>
                this.isUserAuthenticated = isUserAuthenticated
            )
    }

    ngOnDestroy() {
        this._stop$.next();
    }

    onMenuToggled(isToggled: boolean) {
        this.isMenuVisible = isToggled;
    }

    onNavigated(link: MenuLink) {
        if (link.id === 5) {
            this._modal.open();
            return;
        }
        this._router.navigate([link.path]);
    }

    isLinkVisible(link: MenuLink) {
        let isVisible: boolean;
        switch (link.id) {
            case 3:
            case 4:
                isVisible = this.isUserAuthenticated;
                break;
            case 5:
                isVisible = !this.isUserAuthenticated;
                break;
            default:
                isVisible = true;
                break;
        }
        return isVisible;
    }
}
