import {
    Component,
    OnInit,
    ViewEncapsulation,
    HostBinding,
    EventEmitter,
    Output
} from '@angular/core';

@Component({
    selector: 'ari-menu-toggle',
    templateUrl: './menu-toggle.component.html',
    styleUrls: ['./menu-toggle.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class MenuToggleComponent implements OnInit {
    isToggled: boolean;
    numbers: number[];

    @Output() toggled: EventEmitter<boolean>;

    @HostBinding('class.menu-toggle') get cssMenuToggle() {
        return true;
    }

    constructor() {
        this.isToggled = false;
        this.toggled = new EventEmitter();
        this.numbers = [0, 1, 2];
    }

    ngOnInit() {
    }

    onToggled() {
        this.isToggled = !this.isToggled;
        this.toggled.emit(this.isToggled);
    }

}
