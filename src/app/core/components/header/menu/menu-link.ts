export interface MenuLink {
    id: number;
    name: string;
    path: string;
}