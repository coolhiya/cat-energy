import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AuthModule } from 'src/app/main/auth';

import { LogoComponent } from './logo/index';
import { MenuComponent, MenuToggleComponent } from './menu/index';

import { HeaderComponent } from './header.component';


@NgModule({
    declarations: [
        HeaderComponent,
        LogoComponent,
        MenuComponent,
        MenuToggleComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        AuthModule,
        BrowserAnimationsModule
    ],
    exports: [
        HeaderComponent
    ]
})
export class HeaderModule { }
