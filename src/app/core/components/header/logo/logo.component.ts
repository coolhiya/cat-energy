import {
    Component,
    OnInit,
    ViewEncapsulation,
    HostBinding
} from '@angular/core';

@Component({
    selector: 'ari-logo',
    templateUrl: './logo.component.html',
    styleUrls: ['./logo.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class LogoComponent implements OnInit {

    @HostBinding('class.logo') get cssLogo() {
        return true;
    }

    constructor() { }

    ngOnInit() {
    }

}
