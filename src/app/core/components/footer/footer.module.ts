import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MapModule } from 'src/app/core/components/map/index';

import { FooterComponent } from './footer.component';


@NgModule({
    declarations: [
        FooterComponent
    ],
    imports: [
        CommonModule,
        MapModule
    ],
    exports: [
        FooterComponent
    ]
})
export class FooterModule { }
