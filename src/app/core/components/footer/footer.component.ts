import {
    Component,
    OnInit,
    ViewEncapsulation,
    HostBinding
} from '@angular/core';

@Component({
    selector: 'ari-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class FooterComponent implements OnInit {

    @HostBinding('class.footer') get cssFooter() {
        return true;
    }

    constructor() { }

    ngOnInit() {

    }

}
