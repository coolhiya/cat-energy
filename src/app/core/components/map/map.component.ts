import {
    Component,
    OnInit,
    ViewEncapsulation,
    HostBinding
} from '@angular/core';
import * as L from 'leaflet';

@Component({
    selector: 'ari-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class MapComponent implements OnInit {
    private _leaflet: L.Map;

    @HostBinding('class.map') get cssMap() {
        return true;
    }

    constructor() { }

    ngOnInit() {
        this._leaflet = L.map('leaflet').setView([59.921, 30.3097], 11);
        const tileLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
        this._leaflet.addLayer(tileLayer);
    }

}
