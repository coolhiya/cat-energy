import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filter'
})
export class FilterPipe implements PipeTransform {

    transform(value: any[], func: (item: any, filter: any) => boolean, filterName: any): any[] {
        if (!(value && value.length && func && filterName)) {
            return value;
        }
        return value.filter((item: any) => func(item, filterName))
    }

}
