import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeaderModule } from './components/header';
import { FooterModule } from './components/footer';
import { ModalModule } from './components/modal';

@NgModule({
    imports: [
        CommonModule,
        HeaderModule,
        FooterModule,
        ModalModule
    ],
    exports: [
        HeaderModule,
        FooterModule
    ]
})
export class CoreModule { }
